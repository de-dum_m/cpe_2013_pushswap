/*
** my_put_nbr.c for my_put_nbr.c in /home/de-dum_m/rendu/Piscine-C-Jour_03
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Oct  2 18:24:13 2013 de-dum_m
** Last update Thu Dec 26 12:31:47 2013 de-dum_m
*/

#include "my.h"

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_putchar('-', 1);
    }
  if (nb > 9)
    {
     my_put_nbr(nb / 10);
     my_put_nbr(nb % 10);
    }
  else
    my_putchar(nb + 48, 1);
  return (0);
}
