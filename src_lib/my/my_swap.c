/*
** my_swap.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 19:50:26 2013 de-dum_m
** Last update Thu Dec 26 19:53:56 2013 de-dum_m
*/

void	my_swap(int *nb, int *nb2)
{
  int	tmp;

  if (!nb || !nb2)
    return ;
  tmp = *nb;
  *nb = *nb2;
  *nb2 = tmp;
}
