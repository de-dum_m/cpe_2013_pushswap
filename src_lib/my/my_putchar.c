/*
** my_putchar.c for my_putchar.c in /home/de-dum_m/rendu/Piscine-C-lib/my
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue Oct  8 18:38:44 2013 de-dum_m
** Last update Thu Dec 26 10:23:59 2013 de-dum_m
*/

#include <unistd.h>

void	my_putchar(char c, int fd)
{
  write(fd, &c, 1);
}
