/*
** my_strlen.c for my_strlen.c in /home/de-dum_m/rendu/Piscine-C-Jour_04
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Oct  3 10:17:08 2013 de-dum_m
** Last update Sat Dec 21 11:02:16 2013 de-dum_m
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i])
    i++;
  return (i);
}
