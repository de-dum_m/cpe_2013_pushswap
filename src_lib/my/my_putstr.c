/*
** my_putstr.c for my_putstr in /home/de-dum_m/rendu/Piscine-C-Jour_04
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Oct  3 10:05:00 2013 de-dum_m
** Last update Thu Dec 26 10:24:28 2013 de-dum_m
*/

#include "my.h"

int	my_putstr(char *str, int fd)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i], fd);
      i = i + 1;
    }
  return (0);
}
