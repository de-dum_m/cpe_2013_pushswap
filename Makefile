##
## Makefile for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Thu Dec 26 10:10:14 2013 de-dum_m
## Last update Sun Dec 29 20:22:36 2013 de-dum_m
##

CC	= clang

NAME	= push_swap

SRC	= src/push_main.c \
	src/push_usage.c \
	src/push_op_s.c \
	src/push_operations.c \
	src/push_algo.c \
	src/push_list.c

SRO	= $(SRC:.c=.o)

LIBDIR	= src_lib
INCDIR	= includes

MLIB	= src_lib/my

CFLAGS	= -I$(INCDIR) -W -Wall -pedantic -Wextra -g

$(NAME): $(SRO)
	@make -C $(MLIB)
	@$(CC) -o $(NAME) $(SRO) -L$(LIBDIR) -lmy
	@echo -e "\033[32mCompile successful\033[0m"

all:	$(NAME)

clean:
	@rm -f $(SRO)
	make clean -C $(MLIB)

fclean: clean
	rm -f $(NAME)
	make fclean -C $(MLIB)

re:	fclean all

.PHONY: all clean fclean re
