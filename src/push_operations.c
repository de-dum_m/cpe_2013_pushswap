/*
** push_operations.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Dec 28 19:43:44 2013 de-dum_m
** Last update Sun Dec 29 20:14:13 2013 de-dum_m
*/

#include <stdlib.h>
#include "push_struct.h"
#include "push_defines.h"

void	remove_head(t_dl *dl)
{
  if (!dl || !dl->head || !dl->head->next)
    {
      dl->head = NULL;
      return ;
    }
  dl->head = dl->head->next;
  dl->head->prev = NULL;
}

void		rotate_forward(t_dl *dl)
{
  t_elem	*tmp;

  if (!dl->tail || (tmp = malloc(sizeof(t_dl *))) == NULL)
    return ;
  tmp->val = dl->head->val;
  tmp->next = NULL;
  tmp->prev = dl->tail;
  dl->head = dl->head->next;
  dl->head->prev = NULL;
  dl->tail->next = tmp;
  dl->tail = tmp;
}

void		rotate_backwards(t_dl *dl)
{
  t_elem	*tmp;

  if (!dl->tail || (tmp = malloc(sizeof(t_dl *))) == NULL)
    return ;
  tmp->val = dl->tail->val;
  tmp->next = dl->head;
  tmp->prev = NULL;
  dl->tail = dl->tail->prev;
  dl->tail->next = NULL;
  dl->head->prev = tmp;
  dl->head = tmp;
}
