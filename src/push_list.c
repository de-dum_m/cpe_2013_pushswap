/*
** make_list.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:37:33 2013 de-dum_m
** Last update Sun Dec 29 20:03:19 2013 de-dum_m
*/

#include <stdlib.h>
#include "push_struct.h"
#include "push_defines.h"
#include "my.h"

int		print_dl(t_dl *dl)
{
  t_elem	*tmp;

  tmp = dl->head;
  while (tmp != NULL)
    {
      my_put_nbr(tmp->val);
      my_putstr(" > ", 1);
      tmp = tmp->next;
    }
  my_putchar('\n', 1);
  return (SUCCESS);
}

int		my_add_tail(int nb, t_dl *dl)
{
  t_elem	*new;

  if ((new = malloc(sizeof(t_elem))) == NULL)
    return (FAILURE);
  new->val = nb;
  new->next = NULL;
  if (!dl->head)
    {
      new->prev = NULL;
      dl->tail = new;
      dl->head = new;
    }
  else
    {
      dl->tail->next = new;
      new->prev = dl->tail;
      dl->tail = new;
    }
  return (SUCCESS);
}

int		my_add_head(int nb, t_dl *dl)
{
  t_elem	*new;

  if ((new = malloc(sizeof(t_elem))) == NULL)
    return (FAILURE);
  new->val = nb;
  new->prev = NULL;
  if (!dl->head)
    {
      new->next = NULL;
      dl->tail = new;
      dl->head = new;
    }
  else
    {
      dl->head->prev = new;
      new->next = dl->head;
      dl->head = new;
    }
  return (SUCCESS);
}

void	make_list(int ac, char **av, t_dl *dl)
{
  int	i;

  i = 1;
  dl->head = NULL;
  dl->tail = NULL;
  while (i < ac)
    my_add_tail(my_getnbr(av[i++]), dl);
}
