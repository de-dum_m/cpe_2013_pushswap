/*
** push_algo.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Dec 29 20:17:35 2013 de-dum_m
** Last update Tue Dec 31 11:36:05 2013 de-dum_m
*/

#include "push_defines.h"
#include "push_struct.h"
#include "push_funct.h"
#include "my.h"

void	my_sort_b(t_dl *l_a, t_dl *l_b)
{
  if (!l_b->head)
    return ;
  if (l_b->head->val > l_a->head->val)
    {
      while (l_b->head->val > l_a->head->val)
	swap_list(l_a, l_b, RA);
      while (l_b->head && l_a->tail->val < l_b->head->val)
	swap_list(l_a, l_b, PA);
      while (l_a->tail->val < l_a->head->val
	     && (!l_b->head || l_a->tail->val > l_b->head->val))
	swap_list(l_a, l_b, RRA);
    }
}

void	my_sort(t_dl *l_a, t_dl *l_b)
{
  if (!l_a->head)
    {
      while (l_b->head && l_b->head->next)
	swap_list(l_a, l_b, PA);
      swap_list(l_a, l_b, PPA);
      my_putchar('\n', 1);
      return ;
    }
  while (l_a->head->val > l_a->tail->val
	 && (!l_b->head || l_b->head->val < l_a->tail->val))
    swap_list(l_a, l_b, RRA);
  if (l_a->head->next && l_a->head->val > l_a->head->next->val)
    swap_list(l_a, l_b, SA);
  else if (!l_b->head || l_a->head->val > l_b->head->val)
    swap_list(l_a, l_b, PB);
  else
    my_sort_b(l_a, l_b);
  my_sort(l_a, l_b);
}
