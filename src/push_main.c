/*
** push_main.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:13:27 2013 de-dum_m
** Last update Tue Dec 31 10:37:19 2013 de-dum_m
*/

#include <stdlib.h>
#include "push_struct.h"
#include "push_funct.h"
#include "push_defines.h"
#include "my.h"

static int	push_start(int ac, char **av)
{
  t_dl	l_a;
  t_dl	l_b;

  l_b.head = NULL;
  l_b.tail = NULL;
  make_list(ac, av, &l_a);
  my_sort(&l_a, &l_b);
  return (SUCCESS);
}

int	main(int ac, char **av)
{
  if (ac < 2)
    return (my_usage(ARGS));
  push_start(ac, av);
}
