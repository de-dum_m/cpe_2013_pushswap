/*
** push_swaps.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 13:03:24 2013 de-dum_m
** Last update Tue Dec 31 11:36:21 2013 de-dum_m
*/

#include <stdlib.h>
#include "push_struct.h"
#include "push_funct.h"
#include "push_defines.h"
#include "my.h"

void	operation_rr(t_dl *l_a, t_dl *l_b, int mode)
{
  if (mode == RRA)
    {
      rotate_backwards(l_a);
      my_putstr("rra ", 1);
    }
  else if (mode == RRB)
    {
      my_putstr("rrb ", 1);
      rotate_backwards(l_b);
    }
  else if (mode == RRR)
    {
      my_putstr("rrr ", 1);
      rotate_backwards(l_a);
      rotate_backwards(l_b);
    }
}

void	operation_r(t_dl *l_a, t_dl *l_b, int mode)
{
  if (mode == RA)
    {
      my_putstr("ra ", 1);
      rotate_forward(l_a);
    }
  else if (mode == RB)
    {
      my_putstr("rb ", 1);
      rotate_backwards(l_b);
    }
  else if (mode == RR)
    {
      my_putstr("rr ", 1);
      rotate_backwards(l_a);
      rotate_backwards(l_b);
    }
}

void	operation_p(t_dl *l_a, t_dl *l_b, int mode)
{
  if (mode == PA)
    {
      my_putstr("pa ", 1);
      my_add_head(l_b->head->val, l_a);
      remove_head(l_b);
    }
  else if (mode == PPA)
    {
      my_putstr("pa", 1);
      my_add_head(l_b->head->val, l_a);
      remove_head(l_b);
    }
  else if (mode == PB)
    {
      my_putstr("pb ", 1);
      my_add_head(l_a->head->val, l_b);
      remove_head(l_a);
    }
}

void	operation_s(t_dl *l_a, t_dl *l_b, int mode)
{
  if (mode == SA)
    {
      my_putstr("sa ", 1);
      my_swap(&(l_a->head->val), &(l_a->head->next->val));
    }
  else if (mode == SB)
    {
      my_swap(&(l_b->head->val), &(l_b->head->next->val));
      my_putstr("sb ", 1);
    }
  else if (mode == SS)
    {
      my_putstr("ss ", 1);
      my_swap(&(l_a->head->val), &(l_a->head->next->val));
      my_swap(&(l_b->head->val), &(l_b->head->next->val));
    }
}

void	swap_list(t_dl *l_a, t_dl *l_b, int mode)
{
  if (mode <= 3)
    operation_s(l_a, l_b, mode);
  else if (mode <= 6)
    operation_p(l_a, l_b, mode);
  else if (mode <= 9)
    operation_r(l_a, l_b, mode);
  else
    operation_rr(l_a, l_b, mode);
}
