/*
** push_usage.c for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:16:41 2013 de-dum_m
** Last update Thu Dec 26 11:58:38 2013 de-dum_m
*/

#include "push_defines.h"
#include "my.h"

int	my_usage(int error)
{
  my_putstr("Error: ", 2);
  if (error == ARGS)
    my_putstr("insufficient number of args.\n", 2);
  my_putstr("Usage: ./push_swap nb1 nb2 ... nbx\n", 2);
  return (FAILURE);
}
