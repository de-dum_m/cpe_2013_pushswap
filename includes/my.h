/*
** my.h for my.h in /home/de-dum_m/rendu/Piscine-C-Jour_09
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Oct 10 09:59:49 2013 de-dum_m
** Last update Thu Dec 26 19:55:47 2013 de-dum_m
*/

#ifndef MY_H_
#define MY_H_

void	my_putchar(char c, int fd);
void	my_swap(int *nb, int *nb2);
int	my_put_nbr(int nb);
int	my_putstr(char *str, int fd);
int	my_strlen(char *str);
int	my_getnbr(char *str);

#endif /* !MY_H_ */
