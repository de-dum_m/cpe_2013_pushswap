/*
** push_struct.h for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap/includes
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:51:44 2013 de-dum_m
** Last update Sun Dec 29 20:13:42 2013 de-dum_m
*/

#ifndef PUSH_STRUCT_H_
# define PUSH_STRUCT_H_

typedef struct s_elem	t_elem;

struct	s_elem
{
  int		val;
  t_elem	*prev;
  t_elem	*next;
};

typedef struct	s_dl
{
  t_elem	*head;
  t_elem	*tail;
}		t_dl;

#endif /* !PUSH_STRUCT_H_ */
