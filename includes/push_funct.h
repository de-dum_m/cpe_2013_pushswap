/*
** psuh_funct.h for push_swap in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap/includes
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:50:17 2013 de-dum_m
** Last update Sun Dec 29 20:23:21 2013 de-dum_m
*/

#ifndef PUSH_FUNCT_H_
# define PUSH_FUNCT_H_

/* push_usage.c */
int	my_usage(int error);

/* push_list.c */
int	print_dl(t_dl *dl);
void	my_add_head(int nb, t_dl *dl);
void	my_add_tail(int nb, t_dl *dl);
void	remove_head(t_dl *dl);
void	make_list(int ac, char **av, t_dl *dl);

/* push_operations.c */
void	remove_head(t_dl *dl);
void	rotate_forward(t_dl *dl);
void	rotate_backwards(t_dl *dl);

/* push_swaps.c */
void	swap_list(t_dl *l_a, t_dl *l_b, int mode);

/* push_algo.c */
void	my_sort(t_dl *la, t_dl *l_b);

#endif /* !PUSH_FUNCT_H_ */
