/*
** my_push.h for push_swap.c in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:14:54 2013 de-dum_m
** Last update Tue Dec 31 11:35:16 2013 de-dum_m
*/

#ifndef MY_PUSH_H_
# define MY_PUSH_H_

# define SUCCESS	1
# define FAILURE	-1

# define ARGS		-1
# define MALLOC		-2

# define SA		1
# define SB		2
# define SS		3
# define PA		4
# define PB		5
# define PPA		6
# define RA		7
# define RB		8
# define RR		9
# define RRA		10
# define RRB		11
# define RRR		12

#endif /* !MY_PUSH_H_ */
